#Louis Merlaud
#19/10/2020
#ce script vise à avoir les information élémentaire concernant la machine et l'OS

"nom de l'apareil :"
$(Get-WmiObject Win32_OperatingSystem).CSName 
"nom de l'OS :"
$(Get-WMIObject win32_operatingsystem).name
"version de l'OS :"
((systeminfo | findstr [0-9]..)[1]).Split()[27]

"adresse ip et backloop :"
$(Get-NetIPAddress -AddressState Preferred -AddressFamily IPv4).IPAddress
"date et heure d'allumage :"
 $(Get-CimInstance -ClassName Win32_OperatingSystem).LastBootUpTime

 "si l'OS est a jour"
 $updateObject = New-Object -ComObject Microsoft.Update.Session
 $updateObject.ClientApplicationID = "Serverfault Example Script"
 $updateSearcher = $updateObject.CreateUpdateSearcher()
 $searchResults = $updateSearcher.Search("IsInstalled=0")
 if ($searchResults = "0") {
   "Windows est a jour:"
 }else{
   "Windows n'est pas a jour:"
 }
 $a = $(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).sum /1MB
$b = $(Get-Ciminstance Win32_OperatingSystem).FreePhysicalMemory1KB/1MB
$c = $a-$b #on fait la différence entre la ram totale et la ram pour avoir la ram occupé
Write-Host "RAM: " 
Write-Host " Used: " -nonewline; Write-Host  "$c Mo"
Write-Host "Free:" -nonewline; (Get-WmiObject -Class Win32_logicaldisk).FreeSpace

$l = $(Get-WmiObject -Class Win32_logicaldisk).Size1KB/1TB
$e = $(Get-WmiObject -Class Win32_logicaldisk).FreeSpace1KB/1TB
$o = $l-$e  #on fait la différence entre l'espace totale et l'espace libre pour avoir l'espace occupé
Write-Host "Disk: " 
Write-Host " Used: " -nonewline; Write-Host  "$o Go"
Write-Host " Free: " -nonewline; (Get-WmiObject -Class Win32_logicaldisk).FreeSpace

"utilisateur de la machine:"
Write-Host "User list: " -nonewline; Write-Host "$((Get-WmiObject Win32_UserAccount).Name)"

"temps de reponse vers 8.8.8.8"
$(((ping 8.8.8.8 | findstr [0-9]..)[7]).Split()[-1])