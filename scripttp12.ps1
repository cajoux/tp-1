#Louis Merlaud
#24/10/2020
#ce script vérouille la session après 30 sec ou étein le pc après 75 sec

[int] $decompte = $args[1]

#permet d'éteindre le pc après un certain nombre de seconde entré dans l'argument 1
if ($args[0] = "shutdown"){
    Start-Sleep -Seconds $decompte | Stop-Computer
} 
#permet de verrouiller la session après un certain nombre de seconde entré dans l'argument 1
if ($args[0] = "lock"){
    Start-Sleep -Seconds $decompte | rundll32.exe user32.dll,LockWorkStation
}