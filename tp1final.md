Maitrise de poste – Day 1

HOST OS
-
Nom de la machine : *(Get-WmiObject Win32_OperatingSystem).CSName*
```
PORTABLE-ACER-L
```
OS et version : *(Get-WMIObject win32_operatingsystem).name*
```
Microsoft Windows 10 Famille|C:\WINDOWS|\Device\Harddisk0\Partition4)
```
Architecture processeur : *$ENV:PROCESSOR_ARCHITECTURE*
```
AMD64
```
Quantité RAM et modèle de la RAM : *Get-CimInstance win32_physicalmemory | Format-Table Manufacturer,Banklabel,Configuredclockspeed,Devicelocator,Capacity,Serialnumber -autosize*
```
Manufacturer Banklabel Configuredclockspeed Devicelocator   Capacity Serialnumber
------------ --------- -------------------- -------------   -------- ------------
Kingston     CHANNEL A                  933 DIMM 0        8589934592 241B551F
```
DEVICE
-
la marque et le modèle de votre processeur : *wmic cpu get name*
Name
```
AMD A9-9410 RADEON R5, 5 COMPUTE CORES 2C+3G
```
Nombre de cœur/processeur : *get-wmiobject win32_processor | select-object NumberOfCores, NumberOfLogicalProcessors*
```
NumberOfCores  NumberOfLogicalProcessors
-------------  -------------------------
            2                         2
```
Marque et modèle : *wmic computersystem get model,name,manufacturer,systemtype*
```
Manufacturer  Model           Name             SystemType
Acer          Aspire E5-523G  PORTABLE-ACER-L  x64-based PC
```
Disque dur : 
Marque : Samsung
Partition du disque dur : *get disk*
*List disk > select disk 0 > list partition*
 ```
 N° partition   Type              Taille   Décalage
  -------------  ----------------  -------  --------
  Partition 1    Récupération       500 M   1024 K
  Partition 2    Système            100 M    501 M
  Partition 3    Réservé             16 M    601 M
  Partition 4    Principale         464 G    617 M
  Partition 5    Récupération       879 M    464 G
```
---------------------------------------------------
Partition 1 : Partition de récupération, contient la sauvegarde du système d’exploitation et des logiciels installés
Partition 2 : Partition du système, contient le système d’exploitation
Partition 3 : Partition réservé, elle contient des fichiers nécessaires au démarrage du pc
Partition 4 : partition principale, contient l’OS, c’est ici qu’il est installé et démarré
Partition 5 : partition de récupération, contient tout les dossier et logiciels présent sur le disque

--------------------------------------------------
Carte graphique : *wmic path win32_VideoController get name*
```
Name
AMD Radeon (TM) R5 M430
AMD Radeon(TM) R5 Graphics
```
Touchpad : *Get-PnpDevice*
```
OK         Mouse           ELAN I2C Filter Driver                                                           HID\ELAN...
```
USER
-
Utilisateur : 
*wmic useraccount list full*
```
AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=PORTABLE-ACER-L
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3363044416-2795238756-2164914253-500
SIDType=1
Status=Degraded
```
```
AccountType=512
Description=Compte utilisateur géré par le système.
Disabled=TRUE
Domain=PORTABLE-ACER-L
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=DefaultAccount
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-3363044416-2795238756-2164914253-503
SIDType=1
Status=Degraded
```
```
AccountType=512
Description=Compte d'utilisateur invité
Disabled=TRUE
Domain=PORTABLE-ACER-L
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Invité
PasswordChangeable=FALSE
PasswordExpires=FALSE
PasswordRequired=FALSE
SID=S-1-5-21-3363044416-2795238756-2164914253-501
SIDType=1
Status=Degraded
```
```
AccountType=512
Description=
Disabled=FALSE
Domain=PORTABLE-ACER-L
FullName=louis merlaud
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=louis
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3363044416-2795238756-2164914253-1001
SIDType=1
Status=OK
```
```
AccountType=512
Description=
Disabled=FALSE
Domain=PORTABLE-ACER-L
FullName=Luc Merlaud
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=lucme
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3363044416-2795238756-2164914253-1002
SIDType=1
Status=OK
```
```
AccountType=512
Description=Compte d'utilisateur géré et utilisé par le système pour les scénarios Windows Defender Application Guard.
Disabled=TRUE
Domain=PORTABLE-ACER-L
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=WDAGUtilityAccount
PasswordChangeable=TRUE
PasswordExpires=TRUE
PasswordRequired=TRUE
SID=S-1-5-21-3363044416-2795238756-2164914253-504
SIDType=1
Status=Degraded
```

Admin :
 ```
 AccountType=512
Description=Compte d'utilisateur d'administration
Disabled=TRUE
Domain=PORTABLE-ACER-L
FullName=
InstallDate=
LocalAccount=TRUE
Lockout=FALSE
Name=Administrateur
PasswordChangeable=TRUE
PasswordExpires=FALSE
PasswordRequired=TRUE
SID=S-1-5-21-3363044416-2795238756-2164914253-500
SIDType=1
Status=Degraded
```
PROCESSUS :
-
*tasklist*
```
Nom de l’image                 PID Nom de la sessio Numéro de s Utilisation
========================= ======== ================ =========== ============
System Idle Process              0 Services                   0         8 Ko
System                           4 Services                   0     7 080 Ko
Registry                        88 Services                   0    88 280 Ko
smss.exe                       368 Services                   0     1 064 Ko
csrss.exe                      564 Services                   0     5 432 Ko
wininit.exe                    704 Services                   0     6 988 Ko
csrss.exe                      720 Console                    1     5 396 Ko
winlogon.exe                   800 Console                    1    11 568 Ko
services.exe                   840 Services                   0    10 348 Ko
lsass.exe                      848 Services                   0    23 372 Ko
svchost.exe                    960 Services                   0     3 912 Ko
fontdrvhost.exe                976 Console                    1     4 940 Ko
fontdrvhost.exe                972 Services                   0     3 416 Ko
svchost.exe                    400 Services                   0    32 524 Ko
svchost.exe                    460 Services                   0    15 540 Ko
svchost.exe                    768 Services                   0     8 092 Ko
dwm.exe                       1068 Console                    1    63 296 Ko
svchost.exe                   1172 Services                   0     6 068 Ko
svchost.exe                   1216 Services                   0     9 060 Ko
svchost.exe                   1224 Services                   0    11 752 Ko
svchost.exe                   1248 Services                   0    11 780 Ko
svchost.exe                   1264 Services                   0    10 976 Ko
svchost.exe                   1328 Services                   0    12 468 Ko
svchost.exe                   1416 Services                   0     7 412 Ko
svchost.exe                   1516 Services                   0     8 804 Ko
svchost.exe                   1540 Services                   0     6 092 Ko
svchost.exe                   1584 Services                   0     7 832 Ko
svchost.exe                   1596 Services                   0    22 292 Ko
svchost.exe                   1716 Services                   0    13 008 Ko
svchost.exe                   1736 Services                   0     9 732 Ko
svchost.exe                   1792 Services                   0    20 692 Ko
svchost.exe                   1880 Services                   0     8 220 Ko
svchost.exe                   1900 Services                   0    10 712 Ko
svchost.exe                   1908 Services                   0     7 768 Ko
atiesrxx.exe                  1916 Services                   0     6 144 Ko
svchost.exe                   1928 Services                   0    10 532 Ko
svchost.exe                   1972 Services                   0     9 024 Ko
svchost.exe                    552 Services                   0     9 820 Ko
svchost.exe                   1828 Services                   0    16 356 Ko
dasHost.exe                   2012 Services                   0    18 492 Ko
svchost.exe                   2096 Services                   0    12 464 Ko
svchost.exe                   2140 Services                   0    11 628 Ko
svchost.exe                   2180 Services                   0     8 356 Ko
svchost.exe                   2300 Services                   0     9 192 Ko
svchost.exe                   2348 Services                   0     9 460 Ko
svchost.exe                   2416 Services                   0     8 008 Ko
svchost.exe                   2464 Services                   0     8 824 Ko
dasHost.exe                   2752 Services                   0     4 228 Ko
svchost.exe                   2888 Services                   0   115 752 Ko
atieclxx.exe                  2932 Console                    1    10 584 Ko
svchost.exe                   2948 Services                   0     7 996 Ko
svchost.exe                   2956 Services                   0    13 620 Ko
svchost.exe                   2968 Services                   0     5 860 Ko
Memory Compression            3044 Services                   0    47 380 Ko
svchost.exe                   2168 Services                   0     8 548 Ko
svchost.exe                   2732 Services                   0     8 272 Ko
svchost.exe                   3124 Services                   0    19 516 Ko
audiodg.exe                   3204 Services                   0    25 108 Ko
svchost.exe                   3228 Services                   0     6 640 Ko
svchost.exe                   3240 Services                   0    10 424 Ko
svchost.exe                   3360 Services                   0     7 708 Ko
svchost.exe                   3444 Services                   0    20 888 Ko
svchost.exe                   3496 Services                   0    12 904 Ko
spoolsv.exe                   3568 Services                   0    30 308 Ko
svchost.exe                   3600 Services                   0    23 732 Ko
svchost.exe                   3620 Services                   0     8 340 Ko
armsvc.exe                    3768 Services                   0     6 544 Ko
AdminService.exe              3776 Services                   0     8 288 Ko
svchost.exe                   3800 Services                   0    31 076 Ko
mDNSResponder.exe             3808 Services                   0     7 240 Ko
svchost.exe                   3844 Services                   0    34 688 Ko
svchost.exe                   3860 Services                   0    22 868 Ko
svchost.exe                   3896 Services                   0    17 328 Ko
HPPrintScanDoctorService.     3944 Services                   0     8 952 Ko
svchost.exe                   3952 Services                   0     8 016 Ko
mdm.exe                       4012 Services                   0     8 572 Ko
LMIGuardianSvc.exe            4024 Services                   0     9 152 Ko
svchost.exe                   3140 Services                   0    14 344 Ko
svchost.exe                   3152 Services                   0     6 712 Ko
svchost.exe                   3352 Services                   0     9 088 Ko
svchost.exe                   3376 Services                   0     5 660 Ko
MsMpEng.exe                   3300 Services                   0   162 484 Ko
TeamViewer_Service.exe        3628 Services                   0    20 364 Ko
svchost.exe                   4100 Services                   0    21 204 Ko
svchost.exe                   4108 Services                   0     6 220 Ko
svchost.exe                   4240 Services                   0    12 968 Ko
hamachi-2.exe                 4248 Services                   0    14 896 Ko
svchost.exe                   4460 Services                   0    12 556 Ko
svchost.exe                   5020 Services                   0    28 944 Ko
svchost.exe                   5244 Services                   0     6 828 Ko
svchost.exe                   5428 Services                   0     7 112 Ko
svchost.exe                   5440 Services                   0     9 352 Ko
svchost.exe                   5468 Services                   0    12 184 Ko
svchost.exe                   5524 Services                   0    10 240 Ko
NisSrv.exe                    5928 Services                   0    10 812 Ko
svchost.exe                   2988 Services                   0     9 700 Ko
dllhost.exe                   6152 Services                   0    11 272 Ko
printfilterpipelinesvc.ex     6424 Services                   0    17 392 Ko
sihost.exe                    6684 Console                    1    38 072 Ko
svchost.exe                   6716 Console                    1    42 472 Ko
svchost.exe                   6780 Console                    1    49 816 Ko
taskhostw.exe                 6856 Console                    1    17 980 Ko
svchost.exe                   7024 Services                   0    22 192 Ko
svchost.exe                   7064 Services                   0     8 272 Ko
ctfmon.exe                    7112 Console                    1    15 344 Ko
explorer.exe                  6244 Console                    1   157 148 Ko
svchost.exe                   5356 Services                   0    20 312 Ko
svchost.exe                   7284 Console                    1    37 008 Ko
svchost.exe                   7484 Services                   0    24 444 Ko
WpcMon.exe                    7492 Console                    1    11 460 Ko
svchost.exe                   7800 Console                    1    23 092 Ko
svchost.exe                   7820 Services                   0     5 820 Ko
svchost.exe                   7864 Services                   0     8 612 Ko
svchost.exe                   7968 Services                   0    18 908 Ko
StartMenuExperienceHost.e     7368 Console                    1    72 160 Ko
RuntimeBroker.exe             6664 Console                    1    27 652 Ko
RuntimeBroker.exe             8284 Console                    1    38 164 Ko
svchost.exe                   8576 Services                   0    14 036 Ko
SettingSyncHost.exe           8724 Console                    1    19 304 Ko
YourPhone.exe                 8756 Console                    1    52 340 Ko
AvastBrowserCrashHandler.     8032 Services                   0       624 Ko
AvastBrowserCrashHandler6     6932 Services                   0       552 Ko
SecurityHealthSystray.exe     7304 Console                    1     8 928 Ko
SecurityHealthService.exe     9228 Services                   0    14 432 Ko
RAVCpl64.exe                  9336 Console                    1    36 072 Ko
RuntimeBroker.exe             9476 Console                    1    13 768 Ko
SetPoint.exe                  9520 Console                    1    30 956 Ko
RuntimeBroker.exe             9548 Console                    1    23 080 Ko
KHALMNPR.exe                  9596 Console                    1    12 080 Ko
jusched.exe                  10116 Console                    1     7 500 Ko
SearchUI.exe                  8840 Console                    1   184 612 Ko
LULnchr.exe                  10040 Console                    1    12 896 Ko
LogitechUpdate.exe           10168 Console                    1    34 748 Ko
RuntimeBroker.exe             4532 Console                    1    28 156 Ko
svchost.exe                   7976 Services                   0    77 116 Ko
ShellExperienceHost.exe       6880 Console                    1    70 340 Ko
RuntimeBroker.exe             9064 Console                    1    24 508 Ko
SystemSettingsBroker.exe      8328 Console                    1    29 500 Ko
svchost.exe                   2856 Services                   0     8 972 Ko
ApplicationFrameHost.exe     10768 Console                    1    41 156 Ko
WinStore.App.exe             10792 Console                    1     1 780 Ko
SgrmBroker.exe               11252 Services                   0     6 028 Ko
svchost.exe                   6980 Services                   0    16 424 Ko
svchost.exe                   6488 Services                   0     9 884 Ko
svchost.exe                   8988 Services                   0    22 924 Ko
Discord.exe                  11112 Console                    1    64 628 Ko
Discord.exe                  11248 Console                    1    61 240 Ko
Discord.exe                   8448 Console                    1    67 428 Ko
Discord.exe                    584 Console                    1    13 544 Ko
Discord.exe                   9744 Console                    1   219 576 Ko
Discord.exe                  11048 Console                    1    18 508 Ko
RemindersServer.exe           7772 Console                    1    14 932 Ko
OfficeClickToRun.exe         10688 Services                   0    65 712 Ko
svchost.exe                  10668 Services                   0    14 032 Ko
dllhost.exe                   8648 Console                    1    11 820 Ko
powershell.exe               10148 Console                    1    93 676 Ko
conhost.exe                   3132 Console                    1    15 804 Ko
Music.UI.exe                  1940 Console                    1   110 484 Ko
RuntimeBroker.exe            11404 Console                    1    38 708 Ko
AppVShNotify.exe             11732 Services                   0     8 156 Ko
AppVShNotify.exe             11744 Console                    1     8 144 Ko
SystemSettings.exe           11368 Console                    1     9 776 Ko
UserOOBEBroker.exe            9152 Console                    1     8 628 Ko
svchost.exe                   1280 Services                   0    12 284 Ko
SearchIndexer.exe            11920 Services                   0    36 748 Ko
WindowsInternal.Composabl    11116 Console                    1    44 484 Ko
notepad.exe                  11644 Console                    1    16 808 Ko
chrome.exe                    7584 Console                    1   145 488 Ko
chrome.exe                    2896 Console                    1     6 676 Ko
chrome.exe                    9360 Console                    1    57 304 Ko
chrome.exe                   12284 Console                    1    37 848 Ko
chrome.exe                   11300 Console                    1    14 812 Ko
chrome.exe                    6352 Console                    1   164 696 Ko
chrome.exe                    6904 Console                    1    39 232 Ko
svchost.exe                  11796 Services                   0     8 092 Ko
svchost.exe                  12020 Services                   0    11 136 Ko
chrome.exe                    6812 Console                    1   100 112 Ko
chrome.exe                    3920 Console                    1    85 600 Ko
chrome.exe                   12148 Console                    1    49 328 Ko
chrome.exe                   10708 Console                    1    21 168 Ko
WmiPrvSE.exe                  5448 Services                   0    10 848 Ko
svchost.exe                   7084 Services                   0     8 540 Ko
usocoreworker.exe            13276 Services                   0    38 636 Ko
MicrosoftEdgeUpdate.exe      12552 Services                   0     1 532 Ko
Microsoft.Photos.exe         12988 Console                    1    65 048 Ko
RuntimeBroker.exe             8848 Console                    1    17 352 Ko
RuntimeBroker.exe             7436 Console                    1    18 992 Ko
tasklist.exe                 12700 Console                    1     8 628 Ko
```

5 services services systèmes : 

---------------------------------------------------
Interface graphique : c’est ce qui permet à l’homme de communiqué à la machine, tout ce qui est de l’ordre des fenêtres ou du pointeur de la souris fait partie de l’interface graphique. 

Démon réseau : c’est un programme réseaux qui s’exécute en arrière plan et qui est directement contrôlé par l’administrateur
 
lsass.exe :identifie des utilisateur 

svchost.exe : sert de d'hôte pour des applications

dllhost.exe : sert à gérer les librairie dynamique

---------------------------------------------------

Liste des cartes réseaux : 
*Get-NetAdapter | fl Name, InterfaceIndex*
```
Name           : Connexion réseau Bluetooth
InterfaceIndex : 18

Name           : Hamachi
InterfaceIndex : 14

Name           : Ethernet
InterfaceIndex : 13

Name           : Wi-Fi
InterfaceIndex : 6
```
---------------------------------------------------
Carte wifi : carte compatible avec un réseaux wifi équipé d’une antenne émettrice/réceptrice

Carte Ethernet : compatible avec un réseau Ethernet 
Carte de connexion Bluetooth : permet à l’appareille de ce connecter en réseaux sans fil à d’autre appareille et inversement 

Hamachi : logiciel récupérant l’adresse ip de la box afin de permettre à d’autre utilisateur de se connecter au réseau

---------------------------------------------------
Port ptc et udp
*netstat –ano*
```
Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       468
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       5160
  TCP    0.0.0.0:5357           0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:7680           0.0.0.0:0              LISTENING       11836
  TCP    0.0.0.0:49664          0.0.0.0:0              LISTENING       892
  TCP    0.0.0.0:49665          0.0.0.0:0              LISTENING       748
  TCP    0.0.0.0:49666          0.0.0.0:0              LISTENING       1636
  TCP    0.0.0.0:49667          0.0.0.0:0              LISTENING       1384
  TCP    0.0.0.0:49668          0.0.0.0:0              LISTENING       3768
  TCP    0.0.0.0:49707          0.0.0.0:0              LISTENING       880
  TCP    127.0.0.1:5354         0.0.0.0:0              LISTENING       6388
  TCP    127.0.0.1:5939         0.0.0.0:0              LISTENING       6800
  TCP    127.0.0.1:6463         0.0.0.0:0              LISTENING       1392
  TCP    127.0.0.1:59243        0.0.0.0:0              LISTENING       13276
  TCP    192.168.1.10:139       0.0.0.0:0              LISTENING       4
  TCP    192.168.1.10:64920     40.67.251.132:443      ESTABLISHED     6772
  TCP    192.168.1.10:65096     162.159.135.234:443    ESTABLISHED     4720
  TCP    192.168.1.10:65398     192.0.76.3:443         ESTABLISHED     10564
  TCP    192.168.1.10:65400     192.0.77.2:443         ESTABLISHED     10564
  TCP    192.168.1.10:65404     51.138.106.75:443      ESTABLISHED     9028
  TCP    192.168.1.10:65405     52.47.209.216:1        TIME_WAIT       0
  TCP    192.168.1.10:65406     52.47.209.216:2        TIME_WAIT       0
  TCP    192.168.1.10:65407     52.47.209.216:3        TIME_WAIT       0
  TCP    192.168.1.10:65408     52.47.209.216:4        TIME_WAIT       0
  TCP    192.168.1.10:65409     52.47.209.216:5        TIME_WAIT       0
  TCP    192.168.1.10:65410     52.47.209.216:6        TIME_WAIT       0
  TCP    192.168.1.10:65411     52.47.209.216:7        TIME_WAIT       0
  TCP    192.168.1.10:65412     52.47.209.216:8        TIME_WAIT       0
  TCP    192.168.1.10:65413     52.47.209.216:9        TIME_WAIT       0
  TCP    192.168.1.10:65414     52.47.209.216:10       TIME_WAIT       0
  TCP    192.168.1.10:65415     52.47.209.216:11       TIME_WAIT       0
  TCP    192.168.1.10:65416     52.47.209.216:12       TIME_WAIT       0
  TCP    192.168.1.10:65417     52.47.209.216:13       TIME_WAIT       0
  TCP    192.168.1.10:65418     52.47.209.216:14       TIME_WAIT       0
  TCP    192.168.1.10:65419     52.47.209.216:15       TIME_WAIT       0
  TCP    192.168.1.10:65420     52.47.209.216:16       TIME_WAIT       0
  TCP    192.168.1.10:65421     52.47.209.216:17       TIME_WAIT       0
  TCP    192.168.1.10:65422     52.47.209.216:18       TIME_WAIT       0
  TCP    192.168.1.10:65423     52.47.209.216:19       TIME_WAIT       0
  TCP    192.168.1.10:65424     52.47.209.216:20       TIME_WAIT       0
  TCP    192.168.1.10:65425     52.47.209.216:21       TIME_WAIT       0
  TCP    192.168.1.10:65426     52.47.209.216:22       TIME_WAIT       0
  TCP    192.168.1.10:65427     52.47.209.216:23       TIME_WAIT       0
  TCP    192.168.1.10:65428     52.47.209.216:24       TIME_WAIT       0
  TCP    192.168.1.10:65430     52.47.209.216:26       TIME_WAIT       0
  TCP    192.168.1.10:65431     52.47.209.216:27       TIME_WAIT       0
  TCP    192.168.1.10:65432     52.47.209.216:28       TIME_WAIT       0
  TCP    192.168.1.10:65433     52.47.209.216:29       TIME_WAIT       0
  TCP    192.168.1.10:65434     52.47.209.216:30       TIME_WAIT       0
  TCP    192.168.1.10:65463     52.47.209.216:59       CLOSE_WAIT      9252
  TCP    192.168.1.10:65475     52.47.209.216:12       CLOSE_WAIT      9252
  TCP    192.168.1.10:65477     52.114.75.78:443       TIME_WAIT       0
  TCP    192.168.1.10:65478     213.186.33.2:80        CLOSE_WAIT      10564
  TCP    192.168.1.10:65479     213.186.33.2:80        CLOSE_WAIT      10564
  TCP    192.168.1.10:65481     213.186.33.2:80        CLOSE_WAIT      10564
  TCP    192.168.1.10:65482     213.186.33.2:80        CLOSE_WAIT      10564
  TCP    192.168.1.10:65483     213.186.33.2:80        ESTABLISHED     10564
  TCP    192.168.1.10:65484     213.186.33.2:80        CLOSE_WAIT      10564
  TCP    192.168.1.10:65494     79.98.96.110:80        ESTABLISHED     10564
  TCP    192.168.1.10:65495     87.98.224.187:80       CLOSE_WAIT      10564
  TCP    192.168.1.10:65496     87.98.224.187:443      ESTABLISHED     10564
  TCP    [::]:135               [::]:0                 LISTENING       468
  TCP    [::]:445               [::]:0                 LISTENING       4
  TCP    [::]:5357              [::]:0                 LISTENING       4
  TCP    [::]:7680              [::]:0                 LISTENING       11836
  TCP    [::]:49664             [::]:0                 LISTENING       892
  TCP    [::]:49665             [::]:0                 LISTENING       748
  TCP    [::]:49666             [::]:0                 LISTENING       1636
  TCP    [::]:49667             [::]:0                 LISTENING       1384
  TCP    [::]:49668             [::]:0                 LISTENING       3768
  TCP    [::]:49707             [::]:0                 LISTENING       880
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:64977  [2a02:26f0:1b00:192::4106]:443  CLOSE_WAIT      2892
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65082  [2a00:1450:400c:c09::bc]:5228  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65149  [2606:2800:133:206e:1315:22a5:2006:24fd]:443  CLOSE_WAIT      13176
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65396  [2001:41d0:301::21]:443  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65401  [2001:8d8:100f:f000::2e5]:443  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65403  [2a04:fa87:fffe::c000:4902]:443  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65480  [2a00:1450:4007:806::200a]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65485  [2a00:1450:4007:807::2002]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65486  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65487  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65488  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65489  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65490  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65491  [2a04:fa87:fffe::c000:4902]:80  ESTABLISHED     10564
  TCP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:65497  [2a03:2880:f042:10:face:b00c:0:3]:443  ESTABLISHED     10564
  UDP    0.0.0.0:500            *:*                                    6528
  UDP    0.0.0.0:3702           *:*                                    12544
  UDP    0.0.0.0:3702           *:*                                    2020
  UDP    0.0.0.0:3702           *:*                                    2020
  UDP    0.0.0.0:3702           *:*                                    12544
  UDP    0.0.0.0:4500           *:*                                    6528
  UDP    0.0.0.0:5050           *:*                                    5160
  UDP    0.0.0.0:5353           *:*                                    2380
  UDP    0.0.0.0:5353           *:*                                    5972
  UDP    0.0.0.0:5353           *:*                                    5972
  UDP    0.0.0.0:5355           *:*                                    2380
  UDP    0.0.0.0:49666          *:*                                    2020
  UDP    0.0.0.0:49731          *:*                                    3768
  UDP    0.0.0.0:50534          *:*                                    10564
  UDP    0.0.0.0:50685          *:*                                    10564
  UDP    0.0.0.0:52212          *:*                                    10564
  UDP    0.0.0.0:53484          *:*                                    12544
  UDP    0.0.0.0:54706          *:*                                    10564
  UDP    0.0.0.0:55154          *:*                                    10564
  UDP    0.0.0.0:55976          *:*                                    10564
  UDP    0.0.0.0:55977          *:*                                    10564
  UDP    0.0.0.0:56458          *:*                                    10564
  UDP    0.0.0.0:57294          *:*                                    10564
  UDP    0.0.0.0:57673          *:*                                    10564
  UDP    0.0.0.0:57788          *:*                                    10564
  UDP    0.0.0.0:58288          *:*                                    10564
  UDP    0.0.0.0:58289          *:*                                    10564
  UDP    0.0.0.0:58290          *:*                                    10564
  UDP    0.0.0.0:59508          *:*                                    10564
  UDP    0.0.0.0:61456          *:*                                    10564
  UDP    0.0.0.0:61457          *:*                                    10564
  UDP    0.0.0.0:61699          *:*                                    6800
  UDP    0.0.0.0:62335          *:*                                    6388
  UDP    0.0.0.0:62530          *:*                                    10564
  UDP    0.0.0.0:63031          *:*                                    10564
  UDP    0.0.0.0:63034          *:*                                    10564
  UDP    0.0.0.0:63035          *:*                                    10564
  UDP    0.0.0.0:64816          *:*                                    10564
  UDP    10.33.18.21:5353       *:*                                    6800
  UDP    127.0.0.1:1900         *:*                                    2280
  UDP    127.0.0.1:56301        *:*                                    2280
  UDP    127.0.0.1:56974        *:*                                    6548
  UDP    127.0.0.1:61970        *:*                                    2108
  UDP    192.168.1.10:137       *:*                                    4
  UDP    192.168.1.10:138       *:*                                    4
  UDP    192.168.1.10:1900      *:*                                    2280
  UDP    192.168.1.10:2177      *:*                                    9192
  UDP    192.168.1.10:5353      *:*                                    6388
  UDP    192.168.1.10:56300     *:*                                    2280
  UDP    [::]:500               *:*                                    6528
  UDP    [::]:3702              *:*                                    2020
  UDP    [::]:3702              *:*                                    12544
  UDP    [::]:3702              *:*                                    12544
  UDP    [::]:3702              *:*                                    2020
  UDP    [::]:4500              *:*                                    6528
  UDP    [::]:5353              *:*                                    5972
  UDP    [::]:5353              *:*                                    2380
  UDP    [::]:5355              *:*                                    2380
  UDP    [::]:49667             *:*                                    2020
  UDP    [::]:50534             *:*                                    10564
  UDP    [::]:50685             *:*                                    10564
  UDP    [::]:52212             *:*                                    10564
  UDP    [::]:53485             *:*                                    12544
  UDP    [::]:54706             *:*                                    10564
  UDP    [::]:55154             *:*                                    10564
  UDP    [::]:55976             *:*                                    10564
  UDP    [::]:55977             *:*                                    10564
  UDP    [::]:56458             *:*                                    10564
  UDP    [::]:57294             *:*                                    10564
  UDP    [::]:57673             *:*                                    10564
  UDP    [::]:57788             *:*                                    10564
  UDP    [::]:58288             *:*                                    10564
  UDP    [::]:58289             *:*                                    10564
  UDP    [::]:58290             *:*                                    10564
  UDP    [::]:59508             *:*                                    10564
  UDP    [::]:61456             *:*                                    10564
  UDP    [::]:61457             *:*                                    10564
  UDP    [::]:61700             *:*                                    6800
  UDP    [::]:62336             *:*                                    6388
  UDP    [::]:62530             *:*                                    10564
  UDP    [::]:63031             *:*                                    10564
  UDP    [::]:63034             *:*                                    10564
  UDP    [::]:63035             *:*                                    10564
  UDP    [::]:64816             *:*                                    10564
  UDP    [::1]:1900             *:*                                    2280
  UDP    [::1]:5353             *:*                                    6800
  UDP    [::1]:5353             *:*                                    6388
  UDP    [::1]:56299            *:*                                    2280
  UDP    [2001:861:3102:f630:4a:ed12:67c5:2229]:2177  *:*                                    9192
  UDP    [2001:861:3102:f630:bdad:88b6:f738:1e8f]:2177  *:*                                    9192
  UDP    [fe80::4a:ed12:67c5:2229%6]:1900  *:*                                    2280
  UDP    [fe80::4a:ed12:67c5:2229%6]:2177  *:*                                    9192
  UDP    [fe80::4a:ed12:67c5:2229%6]:56298  *:*                                    2280
```
De manière générale ce sont les logiciels Port qui utilise les ports TCP UDP

Ces logiciels servent à exécuter plusieurs logiciels serveur ou plusieurs service sur une même machine    

on peut aussi utiliser : *netstat -a -b -o*

```
Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    0.0.0.0:135            Portable-Acer-Louis:0  LISTENING       356
  RpcSs
 [svchost.exe]
  TCP    0.0.0.0:445            Portable-Acer-Louis:0  LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:5040           Portable-Acer-Louis:0  LISTENING       4560
  CDPSvc
 [svchost.exe]
  TCP    0.0.0.0:5357           Portable-Acer-Louis:0  LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:7680           Portable-Acer-Louis:0  LISTENING       8244
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49664          Portable-Acer-Louis:0  LISTENING       804
 [lsass.exe]
  TCP    0.0.0.0:49665          Portable-Acer-Louis:0  LISTENING       660
 Impossible d’obtenir les informations de propriétaire
  TCP    0.0.0.0:49666          Portable-Acer-Louis:0  LISTENING       1592
  EventLog
 [svchost.exe]
  TCP    0.0.0.0:49667          Portable-Acer-Louis:0  LISTENING       1380
  Schedule
 [svchost.exe]
  TCP    0.0.0.0:49668          Portable-Acer-Louis:0  LISTENING       4292
 [spoolsv.exe]
  TCP    0.0.0.0:49741          Portable-Acer-Louis:0  LISTENING       796
 Impossible d’obtenir les informations de propriétaire
  TCP    127.0.0.1:5354         Portable-Acer-Louis:0  LISTENING       5080
 [mDNSResponder.exe]
  TCP    127.0.0.1:5939         Portable-Acer-Louis:0  LISTENING       5504
 [TeamViewer_Service.exe]
  TCP    127.0.0.1:6463         Portable-Acer-Louis:0  LISTENING       64
 [Discord.exe]
  TCP    127.0.0.1:59243        Portable-Acer-Louis:0  LISTENING       7364
 [SetPoint.exe]
  TCP    192.168.1.10:139       Portable-Acer-Louis:0  LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    192.168.1.10:55674     40.67.254.36:https     ESTABLISHED     5564
  WpnService
 [svchost.exe]
  TCP    192.168.1.10:56037     162.159.130.234:https  ESTABLISHED     12088
 [Discord.exe]
  TCP    192.168.1.10:56116     52.114.75.149:https    TIME_WAIT       0
  TCP    192.168.1.10:56117     162.159.134.233:https  ESTABLISHED     12088
 [Discord.exe]
  TCP    192.168.1.10:56118     162.159.129.235:https  ESTABLISHED     12088
 [Discord.exe]
  TCP    192.168.1.10:56120     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56121     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56122     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56123     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56124     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56125     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56127     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56128     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56129     20.49.150.241:https    TIME_WAIT       0
  TCP    192.168.1.10:56130     162.159.137.234:https  ESTABLISHED     12088
 [Discord.exe]
  TCP    192.168.1.10:56131     51.138.106.75:https    ESTABLISHED     7704
  CDPUserSvc_30e9674
 [svchost.exe]
  TCP    [::]:135               Portable-Acer-Louis:0  LISTENING       356
  RpcSs
 [svchost.exe]
  TCP    [::]:445               Portable-Acer-Louis:0  LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:5357              Portable-Acer-Louis:0  LISTENING       4
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:7680              Portable-Acer-Louis:0  LISTENING       8244
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49664             Portable-Acer-Louis:0  LISTENING       804
 [lsass.exe]
  TCP    [::]:49665             Portable-Acer-Louis:0  LISTENING       660
 Impossible d’obtenir les informations de propriétaire
  TCP    [::]:49666             Portable-Acer-Louis:0  LISTENING       1592
  EventLog
 [svchost.exe]
  TCP    [::]:49667             Portable-Acer-Louis:0  LISTENING       1380
  Schedule
 [svchost.exe]
  TCP    [::]:49668             Portable-Acer-Louis:0  LISTENING       4292
 [spoolsv.exe]
  TCP    [::]:49741             Portable-Acer-Louis:0  LISTENING       796
 Impossible d’obtenir les informations de propriétaire
  TCP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:55788  g2a02-26f0-1b00-0192-0000-0000-0000-4106:https  CLOSE_WAIT      12208
 [WinStore.App.exe]
  TCP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:55789  g2a02-26f0-1b00-0192-0000-0000-0000-4106:https  CLOSE_WAIT      12208
 [WinStore.App.exe]
  TCP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:55790  g2a02-26f0-1b00-0192-0000-0000-0000-4106:https  CLOSE_WAIT      12208
 [WinStore.App.exe]
  TCP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:55905  g2a02-26f0-2b00-0004-0000-0000-5c7a-bcd1:https  CLOSE_WAIT      3844
 [SearchUI.exe]
  TCP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:56126  [2600:1901:0:47fc::]:https  ESTABLISHED     12088
 [Discord.exe]
  UDP    0.0.0.0:500            *:*                                    5272
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*                                    2016
 [dashost.exe]
  UDP    0.0.0.0:3702           *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:3702           *:*                                    2016
 [dashost.exe]
  UDP    0.0.0.0:3702           *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:4500           *:*                                    5272
  IKEEXT
 [svchost.exe]
  UDP    0.0.0.0:5050           *:*                                    4560
  CDPSvc
 [svchost.exe]
  UDP    0.0.0.0:5353           *:*                                    2292
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:5355           *:*                                    2292
  Dnscache
 [svchost.exe]
  UDP    0.0.0.0:49666          *:*                                    2016
 [dashost.exe]
  UDP    0.0.0.0:49668          *:*                                    4292
 [spoolsv.exe]
  UDP    0.0.0.0:49669          *:*                                    5080
 [mDNSResponder.exe]
  UDP    0.0.0.0:50212          *:*                                    64
 [Discord.exe]
  UDP    0.0.0.0:51383          *:*                                    64
 [Discord.exe]
  UDP    0.0.0.0:56398          *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    0.0.0.0:62218          *:*                                    5504
 [TeamViewer_Service.exe]
  UDP    127.0.0.1:1900         *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    127.0.0.1:49671        *:*                                    5700
  iphlpsvc
 [svchost.exe]
  UDP    127.0.0.1:60189        *:*                                    1868
  NlaSvc
 [svchost.exe]
  UDP    127.0.0.1:63743        *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.10:137       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.10:138       *:*                                    4
 Impossible d’obtenir les informations de propriétaire
  UDP    192.168.1.10:1900      *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.10:2177      *:*                                    788
  QWAVE
 [svchost.exe]
  UDP    192.168.1.10:5353      *:*                                    5080
 [mDNSResponder.exe]
  UDP    192.168.1.10:63742     *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    192.168.1.53:5353      *:*                                    5504
 [TeamViewer_Service.exe]
  UDP    [::]:500               *:*                                    5272
  IKEEXT
 [svchost.exe]
  UDP    [::]:3702              *:*                                    2016
 [dashost.exe]
  UDP    [::]:3702              *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    [::]:3702              *:*                                    2016
 [dashost.exe]
  UDP    [::]:3702              *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    [::]:4500              *:*                                    5272
  IKEEXT
 [svchost.exe]
  UDP    [::]:5353              *:*                                    2292
  Dnscache
 [svchost.exe]
  UDP    [::]:5355              *:*                                    2292
  Dnscache
 [svchost.exe]
  UDP    [::]:49667             *:*                                    2016
 [dashost.exe]
  UDP    [::]:49670             *:*                                    5080
 [mDNSResponder.exe]
  UDP    [::]:56399             *:*                                    6936
  FDResPub
 [svchost.exe]
  UDP    [::]:62219             *:*                                    5504
 [TeamViewer_Service.exe]
  UDP    [::1]:1900             *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    [::1]:5353             *:*                                    5080
 [mDNSResponder.exe]
  UDP    [::1]:5353             *:*                                    5504
 [TeamViewer_Service.exe]
  UDP    [::1]:63741            *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    [2001:861:3102:f630:4a:ed12:67c5:2229]:2177  *:*                                    788
  QWAVE
 [svchost.exe]
  UDP    [2001:861:3102:f630:7c3b:12a8:b9bf:3e47]:2177  *:*                                    788
  QWAVE
 [svchost.exe]
  UDP    [fe80::4a:ed12:67c5:2229%6]:1900  *:*                                    2268
  SSDPSRV
 [svchost.exe]
  UDP    [fe80::4a:ed12:67c5:2229%6]:2177  *:*                                    788
  QWAVE
 [svchost.exe]
  UDP    [fe80::4a:ed12:67c5:2229%6]:63740  *:*                                    2268
  SSDPSRV
 [svchost.exe]
```
on peut trouver certain programme :

chrome.exe : permet de lancer chrome

Discord.exe : permet de lancer discord

TeamViewer_Service.exe : permet de lancer team viewer

svchost.exe : sert de d'hôte pour des applications 

lsass.exe :identifie des utilisateur 

mDNSResponder.exe :Multicast Domain Name System Responder il est utilisé afin que ces programmes puissent trouver des ressources telles que des ordinateurs et des imprimantes sur le réseau local.

setpoint.exe : logiciel de logitech qui permet plusieurs option sur la souris




-------------------------------------------------------------------------------------------------------

II Scripting
-
```
#Louis Merlaud
#19/10/2020
#ce script vise à avoir les information élémentaire concernant la machine et l'OS

"nom de l'apareil :"
$(Get-WmiObject Win32_OperatingSystem).CSName 
"nom de l'OS :"
$(Get-WMIObject win32_operatingsystem).name
"version de l'OS :"
((systeminfo | findstr [0-9]..)[1]).Split()[27]

"adresse ip et backloop :"
$(Get-NetIPAddress -AddressState Preferred -AddressFamily IPv4).IPAddress
"date et heure d'allumage :"
 $(Get-CimInstance -ClassName Win32_OperatingSystem).LastBootUpTime

 "si l'OS est a jour"
 $updateObject = New-Object -ComObject Microsoft.Update.Session
 $updateObject.ClientApplicationID = "Serverfault Example Script"
 $updateSearcher = $updateObject.CreateUpdateSearcher()
 $searchResults = $updateSearcher.Search("IsInstalled=0")
 if ($searchResults = "0") {
   "Windows est a jour:"
 }else{
   "Windows n'est pas a jour:"
 }
 $a = $(Get-CimInstance Win32_PhysicalMemory | Measure-Object -Property capacity -Sum).sum /1MB
$b = $(Get-Ciminstance Win32_OperatingSystem).FreePhysicalMemory1KB/1MB
$c = $a-$b #on fait la différence entre la ram totale et la ram pour avoir la ram occupé
Write-Host "RAM: " 
Write-Host " Used: " -nonewline; Write-Host  "$c Mo"
Write-Host "Free:" -nonewline; (Get-WmiObject -Class Win32_logicaldisk).FreeSpace

$l = $(Get-WmiObject -Class Win32_logicaldisk).Size1KB/1TB
$e = $(Get-WmiObject -Class Win32_logicaldisk).FreeSpace1KB/1TB
$o = $l-$e  #on fait la différence entre l'espace totale et l'espace libre pour avoir l'espace occupé
Write-Host "Disk: " 
Write-Host " Used: " -nonewline; Write-Host  "$o Go"
Write-Host " Free: " -nonewline; (Get-WmiObject -Class Win32_logicaldisk).FreeSpace

"utilisateur de la machine:"
Write-Host "User list: " -nonewline; Write-Host "$((Get-WmiObject Win32_UserAccount).Name)"

"temps de reponse vers 8.8.8.8"
$(((ping 8.8.8.8 | findstr [0-9]..)[7]).Split()[-1])
```
---------------------------------------------------

```
#Louis Merlaud
#24/10/2020
#ce script vérouille la session après 30 sec ou étein le pc après 75 sec

[int] $decompte = $args[1]

#permet d'éteindre le pc après un certain nombre de seconde entré dans l'argument 1
if ($args[0] = "shutdown"){
    Start-Sleep -Seconds $decompte | Stop-Computer
} 
#permet de verrouiller la session après un certain nombre de seconde entré dans l'argument 1
if ($args[0] = "lock"){
    Start-Sleep -Seconds $decompte | rundll32.exe user32.dll,LockWorkStation
}
```

------------------------------------------------------------------------------------------------------


III Gestion de softs
-

par rapport au téléchargement depuis internet, le gestionaire de paquet sert à avoir des transfert de donnés sécurisé si l'on envoie des données sur internet, de plus un gestionnnaire de paquets permet à l'utilisateur de par exemple intaller, mettre à jour ou désinstaller les paquets disponible; il permettent aussi de vérifier l'intégrité des paquet et les dépendances logicielles lié à ces paquets.
cela permet aussi un grand gain de temps
Toutes ces vérification contribue à sécuriser le téléchargement

gestionnaire de paquet utiliser : chocolatey

liste des paquet déjà installés : *choco list*

cela va nous montrer tous les paquets de la machine

pour les paquets installé avec chocolatey : *choco list -l*
```
chocolatey 0.10.15
chocolatey-core.extension 1.3.5.1
chocolatey-dotnetfx.extension 1.0.1
chocolatey-windowsupdate.extension 1.0.4
dotnet4.7.2 4.7.2.20180712
KB2670838 1.0.20181019
KB2919355 1.0.20160915
KB2919442 1.0.20160915
paint.net 4.2.14
```
provenance des paquets : *choco search <filter>* 

grâce à cette fonction on peut retrouver l'origine des paquets par filtration

----------------------------------------------------

IV Partage de Fichiers
-

pour prouver que le partage est actif, on utilise la commande : *Get-WindowsOptionalFeature –Online –FeatureName SMB1Protocol*

```

FeatureName      : SMB1Protocol
DisplayName      : Support de partage de fichiers SMB 1.0/CIFS
Description      : Support du protocole de partage de fichiers SMB 1.0/CIFS et du protocole Explorateur d’ordinateurs.
RestartRequired  : Possible
State            : Enabled
CustomProperties :
                   ServerComponent\Description : Support du protocole de partage de fichiers SMB 1.0/CIFS et du
                   protocole Explorateur d’ordinateurs.
                   ServerComponent\DisplayName : Support de partage de fichiers SMB 1.0/CIFS
                   ServerComponent\Id : 487
                   ServerComponent\Type : Feature
                   ServerComponent\UniqueName : FS-SMB1
                   ServerComponent\Deploys\Update\Name : SMB1Protocol
```

on peut voir que le statut (state) est activé (enable) 

en tapant la commande : *sc.exe qc lanmanworkstation*, on peut voir différente information du client : 
```
[SC] QueryServiceConfig réussite(s)

SERVICE_NAME: lanmanworkstation
        TYPE               : 20  WIN32_SHARE_PROCESS
        START_TYPE         : 2   AUTO_START
        ERROR_CONTROL      : 1   NORMAL
        BINARY_PATH_NAME   : C:\WINDOWS\System32\svchost.exe -k NetworkService -p
        LOAD_ORDER_GROUP   : NetworkProvider
        TAG                : 0
        DISPLAY_NAME       : Station de travail
        DEPENDENCIES       : Bowser
                           : MRxSmb20
                           : NSI
        SERVICE_START_NAME : NT AUTHORITY\NetworkService
```
on peut notament voir ici que le service "svchost.exe" est dédié à cette tache, et nous avons vus plus haut que ce service était lier à plusieurs ports, un de ces ports correspond au partage de fichier

 

pour voir si un client peut y accéder on fait la commande :*Get-SmbServerConfiguration | Select AuditSmb1Access*
```
AuditSmb1Access
---------------
          False
```
ici on vois que le client n'y a pas accès, don con fait cette commande : *Set-SmbServerConfiguration –AuditSmb1Access $true*

et donc si l'on refait la commande précédente : 
```
AuditSmb1Access
---------------
           True
```

depuis la vm : 
-
sur windows en effectue un clic droit sur le fichier que l'on veux > propriété > partage > partager > on choisie les utilisateurs avec lesquels on veut partager le fichier.
on entre la commande  *mount -t cifs -o username=<VOTRE_UTILISATEUR>,password=<VOTRE_MOT_DE_PASSE> //<IP_DE_VOTRE_PC>/<NOM_DU_PARTAGE> /opt/partage pour monter le partage

(de mon côté je n'ai réussit à monter le partage)